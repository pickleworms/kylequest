﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    private Rigidbody KyleRB; //KyleRB is Kyle's Rigid Body. This gives him physics.
    private Animator KyleAnim; // KyleAnim is used to indicate which animation should be playing for the state machine.
    private enum State {idle, walking, jumping, falling} // State machine variables
    private State state = State.idle; //State machine variable is state and set to idle by default. This changes in the StateSwitch function
    private Collider coll; //Collider for ground objects
    [SerializeField] private LayerMask ground; //LayerMask
    [SerializeField] private float speed = 5f; //How fast Kyle moves left and right
    [SerializeField] private float jumpSpeed = 5f; //How fast Kyle jumps

    void Start() //Start is called before the first frame update
    {
        KyleRB = GetComponent<Rigidbody>();
        KyleAnim = GetComponent<Animator>();
        coll = GetComponent<Collider>();
    }
    
    void Update() //Update is called once per frame
    {
        MovementManager();
        StateSwitcher();
        KyleAnim.SetInteger("state", (int)state);

    }

    private void MovementManager()
    {
        float hDirection = Input.GetAxis("Horizontal");

        if (hDirection < 0)
        {
            KyleRB.velocity = new Vector3(-speed, KyleRB.velocity.y, 0);
            transform.localScale = new Vector3(-1, 1, 0);
        }
        else if (hDirection > 0)
        {
            KyleRB.velocity = new Vector3(speed, KyleRB.velocity.y, 0);
            transform.localScale = new Vector3(1, 1, 0);
        }

        if (Input.GetButtonDown("Jump"))
        {
            KyleRB.velocity = new Vector3(KyleRB.velocity.x, jumpSpeed, 0);
            state = State.jumping;
        }
    }
    private void StateSwitcher()
    {
        if(state == State.jumping)
        {
            if (KyleRB.velocity.y < .1f)
            {
                state = State.falling;
            }      
        }
        else if(state == State.falling)
        {
            if(coll.isTrigger)
            {
                state = State.idle;
            }
        }
        else if(Mathf.Abs(KyleRB.velocity.x) > Mathf.Epsilon)
        {
            state = State.walking;
        }
        else
        {
            state = State.idle;
        }
        
    }


}