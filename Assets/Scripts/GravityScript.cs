﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityScript : MonoBehaviour
{
    public float mGravity;
    public float mJumpForce;
    public float mCurrentVelocityDueToGravity;
    public bool mIsGrounded;
    public float mMovementSpeed;
    private enum State { idle, walking, jumping, falling }
    private State state = State.idle;
    private Rigidbody KyleRB;
    public Animator KyleAnim;
    void Start()
    {
        mGravity = -40;
        mJumpForce = 15;
        mMovementSpeed = 5;
        KyleRB = GetComponent<Rigidbody>();
        KyleAnim = GetComponent<Animator>();
    }


    void Update()
    {
        StateSwitcher();
        KyleAnim.SetInteger("state", (int)state);


        if (Input.GetKeyDown(KeyCode.Space))
        {
            mIsGrounded = false;
            mCurrentVelocityDueToGravity = 0;
            mCurrentVelocityDueToGravity += mJumpForce;
            state = State.jumping;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(mMovementSpeed, 0, 0) * Time.deltaTime;
            transform.localScale = new Vector3(1, 1, 0);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-mMovementSpeed, 0, 0) * Time.deltaTime;
            transform.localScale = new Vector3(-1, 1, 0);
        }

        mCurrentVelocityDueToGravity += mGravity * Time.deltaTime;
        if(mIsGrounded == false)
        {
            transform.position += new Vector3(0, mCurrentVelocityDueToGravity, 0) * Time.deltaTime;
        }
    }

       private void OnTriggerEnter(Collider other)
    {
        mIsGrounded = true;
        transform.position = new Vector3(transform.position.x, other.transform.position.y, transform.position.z);
    }

    private void StateSwitcher()
    {
        if (state == State.jumping)
        {
            if (KyleRB.velocity.y < .1f)
            {
                state = State.falling;
            }
        }
        else if (state == State.falling)
        {
            if (state == State.falling)
            {
                state = State.idle;
            }
        }
        else if (Mathf.Abs(KyleRB.velocity.x) > Mathf.Epsilon)
        {
            state = State.walking;
        }
        else
        {
            state = State.idle;
        }

    }
}